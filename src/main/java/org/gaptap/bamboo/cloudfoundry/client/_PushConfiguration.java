package org.gaptap.bamboo.cloudfoundry.client;

import org.gaptap.bamboo.cloudfoundry.Nullable;
import org.immutables.value.Value;

import java.io.File;

@Value.Immutable
abstract class _PushConfiguration {

    abstract File applicationFile();

    @Nullable
    abstract Integer startupTimeout();

    @Nullable
    abstract Integer stagingTimeout();

    @Value.Default
    public boolean start(){
        return true;
    }

    @Value.Default
    public boolean disableUnsettingStaleEnvVars(){
        return false;
    }

    @Value.Default
    public boolean disableUnmappingStaleRoutes(){
        return false;
    }
}
