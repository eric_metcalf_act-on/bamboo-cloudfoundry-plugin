package org.gaptap.bamboo.cloudfoundry.client;

public class ConnectionValidator {

    public boolean isValid(CloudFoundryService cloudFoundryService){
        try {
            cloudFoundryService
                    .validateConnection()
                    .block();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
