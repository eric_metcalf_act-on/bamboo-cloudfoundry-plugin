package org.gaptap.bamboo.cloudfoundry.tasks.config;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.security.EncryptionService;
import com.atlassian.bamboo.task.TaskConfiguratorHelper;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.atlassian.struts.TextProvider;
import com.google.common.collect.ImmutableList;
import org.gaptap.bamboo.cloudfoundry.admin.CloudFoundryAdminService;
import org.gaptap.bamboo.cloudfoundry.client.CloudFoundryServiceFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class TaskTaskConfigurator extends BaseCloudFoundryTaskConfigurator {

    public static final String OPTIONS = "cf_options";
    public static final String SELECTED_OPTION = "cf_option";
    public static final String OPTION_RUN_TASK = "runTask";
    public static final String OPTION_WAIT_ON_TASK = "waitOnTask";

    public static final String RUN_APPLICATION_NAME = "cf_runApplicationName";
    public static final String RUN_COMMAND = "cf_runCommand";
    public static final String RUN_TASK_NAME = "cf_runTaskName";
    public static final String RUN_MEMORY = "cf_runMemory";
    public static final String RUN_ENVIRONMENT = "cf_runEnvironment";
    public static final String RUN_CAPTURE_TASK_ID = "cf_runCaptureTaskId";
    public static final String RUN_TASK_ID_VARIABLE_NAME = "cf_runTaskIdVariableName";

    public static final String WAIT_APPLICATION_NAME = "cf_waitApplicationName";
    public static final String WAIT_TASK_ID = "cf_waitTaskId";
    public static final String WAIT_TIMEOUT = "cf_waitTimeout";
    public static final String WAIT_TERMINATE_TASK_ON_TIMEOUT = "cf_waitTerminateTaskOnTimeout";
    public static final String WAIT_FAIL_ON_TASK_FAILURE = "cf_waitFailOnTaskFailure";

    private static final List<String> FIELDS_TO_COPY = ImmutableList.of(SELECTED_OPTION, RUN_APPLICATION_NAME, RUN_COMMAND,
            RUN_TASK_NAME, RUN_MEMORY, RUN_ENVIRONMENT, RUN_CAPTURE_TASK_ID, RUN_TASK_ID_VARIABLE_NAME,
            WAIT_APPLICATION_NAME, WAIT_TASK_ID, WAIT_TIMEOUT, WAIT_FAIL_ON_TASK_FAILURE, WAIT_TERMINATE_TASK_ON_TIMEOUT);

    public TaskTaskConfigurator(CloudFoundryAdminService adminService, TextProvider textProvider,
                                TaskConfiguratorHelper taskConfiguratorHelper, EncryptionService encryptionService,
                                CloudFoundryServiceFactory cloudFoundryServiceFactory) {
        super(adminService, textProvider, taskConfiguratorHelper, encryptionService, cloudFoundryServiceFactory);
    }

    @Override
    @NotNull
    public Map<String, String> generateTaskConfigMap(@NotNull ActionParametersMap params,
                                                     @Nullable TaskDefinition previousTaskDefinition) {
        Map<String, String> configMap = super.generateTaskConfigMap(params, previousTaskDefinition);
        taskConfiguratorHelper.populateTaskConfigMapWithActionParameters(configMap, params, FIELDS_TO_COPY);
        return configMap;
    }

    @Override
    public void populateContextForCreate(@NotNull Map<String, Object> context) {
        super.populateContextForCreate(context);
        populateContextForAll(context);
        context.put(RUN_TASK_ID_VARIABLE_NAME, "cloudfoundry.task.id");
        context.put(WAIT_TASK_ID, "${bamboo.cloudfoundry.task.id}");
        context.put(WAIT_FAIL_ON_TASK_FAILURE, "true");
        context.put(WAIT_TIMEOUT, "300");
    }

    @Override
    public void populateContextForEdit(@NotNull Map<String, Object> context, @NotNull TaskDefinition taskDefinition) {
        super.populateContextForEdit(context, taskDefinition);
        populateContextForAll(context);
        populateContextForModify(context, taskDefinition);
    }

    private void populateContextForModify(@NotNull Map<String, Object> context, @NotNull TaskDefinition taskDefinition) {
        taskConfiguratorHelper.populateContextWithConfiguration(context, taskDefinition, FIELDS_TO_COPY);
    }

    private void populateContextForAll(@NotNull final Map<String, Object> context) {
        Map<String, String> options = new LinkedHashMap<String, String>();
        options.put(OPTION_RUN_TASK, textProvider.getText("cloudfoundry.task.task.option.runTask"));
        options.put(OPTION_WAIT_ON_TASK, textProvider.getText("cloudfoundry.task.task.option.waitOnTask"));
        context.put(OPTIONS, options);
    }

    @Override
    public void validate(@NotNull ActionParametersMap params, @NotNull ErrorCollection errorCollection) {
        super.validate(params, errorCollection);
        validateRequiredNotBlank(SELECTED_OPTION, params, errorCollection);

        if(OPTION_RUN_TASK.equals(params.getString(SELECTED_OPTION))){
            validateRequiredNotBlank(RUN_APPLICATION_NAME, params, errorCollection);
            validateRequiredNotBlank(RUN_COMMAND, params, errorCollection);
            validateRequiredNotBlank(RUN_TASK_NAME, params, errorCollection);
            validateRequiredNotBlank(RUN_CAPTURE_TASK_ID, params, errorCollection);
            if(params.getBoolean(RUN_CAPTURE_TASK_ID)){
                validateRequiredNotBlank(RUN_TASK_ID_VARIABLE_NAME, params, errorCollection);
            }
        } else  if(OPTION_WAIT_ON_TASK.equals(params.getString(SELECTED_OPTION))){
            validateRequiredNotBlank(WAIT_APPLICATION_NAME, params, errorCollection);
            validateRequiredNotBlank(WAIT_TASK_ID, params, errorCollection);
            validateRequiredNotBlank(WAIT_TIMEOUT, params, errorCollection);
        }
    }
}
