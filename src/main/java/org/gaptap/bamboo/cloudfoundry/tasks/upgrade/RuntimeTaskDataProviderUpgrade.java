/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.tasks.upgrade;

import static com.google.common.collect.Iterables.filter;
import static org.gaptap.bamboo.cloudfoundry.admin.CloudFoundryPredicates.isCloudFoundryTask;
import static org.gaptap.bamboo.cloudfoundry.admin.CloudFoundryPredicates.isJobWithCloudFoundryTask;

import java.util.Collection;
import java.util.Map;

import org.apache.log4j.Logger;
import org.gaptap.bamboo.cloudfoundry.PluginProperties;
import org.gaptap.bamboo.cloudfoundry.tasks.dataprovider.TargetTaskDataProvider;

import com.atlassian.bamboo.build.BuildDefinition;
import com.atlassian.bamboo.build.Job;
import com.atlassian.bamboo.plan.PlanManager;
import com.atlassian.bamboo.task.TaskConfigurationService;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.sal.api.message.Message;
import com.atlassian.sal.api.upgrade.PluginUpgradeTask;
import com.atlassian.spring.container.ContainerManager;
import com.google.common.collect.Maps;

/**
 * See {@link #getShortDescription()}. The addition of RuntimeTaskDataProvider
 * in Bamboo 5 allows us to not store centrally managed data directly within
 * TaskDefinitions. This upgrade removes all the centrally managed data that
 * previously was copied into TaskDefinitions as it is no longer relevant.
 * <p/>
 * Requires Bamboo version >= 4.4, but since this upgrade is for moving to 5.0+
 * that shouldn't be a problem :-).
 * 
 * @author David Ehringer
 */
public class RuntimeTaskDataProviderUpgrade implements PluginUpgradeTask {

    private static final Logger LOG = Logger.getLogger(RuntimeTaskDataProviderUpgrade.class);

    private final PlanManager planManager;
    private TaskConfigurationService taskConfigurationService;

    public RuntimeTaskDataProviderUpgrade(PlanManager planManager) {
        this.planManager = planManager;
    }

    @Override
    public Collection<Message> doUpgrade() throws Exception {
        LOG.info("Executing RuntimeTaskDataProviderUpgrade");

        for (Job job : filter(planManager.getAllPlans(Job.class), isJobWithCloudFoundryTask())) {
            BuildDefinition buildDefinition = job.getBuildDefinition();
            for (TaskDefinition taskDefinition : filter(buildDefinition.getTaskDefinitions(), isCloudFoundryTask())) {
                getTaskConfigurationService().editTask(job.getPlanKey(),
                        taskDefinition.getId(), taskDefinition.getUserDescription(), taskDefinition.isEnabled(),
                        getUpdatedTaskConfigurationMap(taskDefinition), taskDefinition.getRootDirectorySelector());
            }
        }

        LOG.info("RuntimeTaskDataProviderUpgrade Complete");
        return null;
    }

    private Map<String, String> getUpdatedTaskConfigurationMap(TaskDefinition taskDefinition) {
        Map<String, String> config = Maps.newHashMap(taskDefinition.getConfiguration());
        config.remove(TargetTaskDataProvider.TARGET_URL);
        config.remove(TargetTaskDataProvider.USERNAME);
        config.remove(TargetTaskDataProvider.PASSWORD);
        config.remove(TargetTaskDataProvider.PROXY_HOST);
        config.remove(TargetTaskDataProvider.PROXY_PORT);
        return config;
    }

    @Override
    public int getBuildNumber() {
        return 1;
    }

    @Override
    public String getPluginKey() {
        return PluginProperties.getPluginKey();
    }

    @Override
    public String getShortDescription() {
        return "Upgrades Task Configuration Data so that central CF Target information is not stored in the TaskConfiguration.";
    }

    private TaskConfigurationService getTaskConfigurationService() {
        if (taskConfigurationService == null) {
            taskConfigurationService = (TaskConfigurationService) ContainerManager
                    .getComponent("taskConfigurationService");
        }
        return taskConfigurationService;
    }
}
