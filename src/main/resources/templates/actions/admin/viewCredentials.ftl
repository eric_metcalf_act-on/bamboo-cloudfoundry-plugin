
<html>
<head>
	[@ui.header pageKey="cloudfoundry.global.credentials.title" title=true /]
	<meta name="decorator" content="adminpage">
</head>
<body>
	[@ui.header pageKey="cloudfoundry.global.credentials.heading" /]
	<div class="toolbar">
		<div class="aui-toolbar inline">
			<ul class="toolbar-group">
				<li class="toolbar-item">
					<a class="toolbar-trigger" href="[@ww.url action='addCredentials' namespace='/admin/cloudfoundry' /]">[@ww.text name='cloudfoundry.global.add.credential' /]</a>
				</li>
			</ul>
		</div>
	</div>
	<p>[@ww.text name='cloudfoundry.global.credentials.description' /]</p>
	[@ww.actionmessage /]
	[@ui.clear/]
	[@ui.bambooPanel titleKey='cloudfoundry.global.credentials.list.heading']
		[#if credentials!?size > 0]	
		<table id="sonar-servers" class="aui" width="100%">
			<thead><tr>
				<th class="labelPrefixCell">[@ww.text name='cloudfoundry.global.credentials.list.heading.credential' /]</th>
				<th class="valueCell">[@ww.text name='cloudfoundry.global.credentials.list.heading.configuration' /]</th>
				<th class="operations">[@ww.text name='cloudfoundry.global.credentials.list.heading.operations' /]</th>
			</tr></thead>
			<tbody>
				[#foreach credential in credentials]
					<tr>
						<td class="labelPrefixCell">
							${credential.name}<br />
							[#if credential.description?has_content]
								<span class="subGrey">${credential.description}</span>
							[/#if]
						</td>
						<td class="valueCell">
							<b>[@ww.text name='cloudfoundry.global.credentials.list.username' /]:</b> ${credential.username}<br />
						</td>
						<td class="operations">
							<a href="${req.contextPath}/admin/cloudfoundry/editCredentials.action?credentialsId=${credential.ID}">[@ww.text name='cloudfoundry.global.edit.credential' /]</a>
							| <span class="subGrey">[@ww.text name='cloudfoundry.global.delete.credential' /]</span>
						</td>
					</tr>
				[/#foreach]
			</tbody>
		</table>			
		[#else]
			[@ui.messageBox type='warning' titleKey='cloudfoundry.global.credentials.none']
				[@ww.text name='cloudfoundry.global.credentials.none.help' /]
			[/@ui.messageBox]
		[/#if]
	[/@ui.bambooPanel]
</body>
</html>
