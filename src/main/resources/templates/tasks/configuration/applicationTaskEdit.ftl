
[#include "selectTargetFragment.ftl"]

[@ui.bambooSection titleKey='cloudfoundry.task.application.section']

	[@ww.select labelKey="cloudfoundry.task.application.option" name="cf_option" list=cf_options listKey="key" listValue="value" toggle='true' /]
	[@ui.bambooSection dependsOn='cf_option' showOn='list']
		<p>[@ww.text name='cloudfoundry.task.application.list.description' /]</p>
	[/@ui.bambooSection]
	[@ui.bambooSection dependsOn='cf_option' showOn='show']
		<p>[@ww.text name='cloudfoundry.task.application.show.description' /]</p>
	    [@ww.textfield labelKey='cloudfoundry.task.application.showName' descriptionKey='cloudfoundry.task.application.showName.description' name='cf_showName' required='true' /]
	[/@ui.bambooSection]
	[@ui.bambooSection dependsOn='cf_option' showOn='start']
		<p>[@ww.text name='cloudfoundry.task.application.start.description' /]</p>
	    [@ww.textfield labelKey='cloudfoundry.task.application.startName' descriptionKey='cloudfoundry.task.application.startName.description'  name='cf_startName' required='true' /]
	[/@ui.bambooSection]
	[@ui.bambooSection dependsOn='cf_option' showOn='stop']
		<p>[@ww.text name='cloudfoundry.task.application.stop.description' /]</p>
	    [@ww.textfield labelKey='cloudfoundry.task.application.stopName' descriptionKey='cloudfoundry.task.application.stopName.description' name='cf_stopName' required='true' /]
	[/@ui.bambooSection]
	[@ui.bambooSection dependsOn='cf_option' showOn='restart']
		<p>[@ww.text name='cloudfoundry.task.application.restart.description' /]</p>
	    [@ww.textfield labelKey='cloudfoundry.task.application.restartName' descriptionKey='cloudfoundry.task.application.restartName.description' name='cf_restartName' required='true' /]
	[/@ui.bambooSection]
	[@ui.bambooSection dependsOn='cf_option' showOn='delete']
		<p>[@ww.text name='cloudfoundry.task.application.delete.description' /]</p>
	    [@ww.textfield labelKey='cloudfoundry.task.application.deleteName' descriptionKey='cloudfoundry.task.application.deleteName.description' name='cf_deleteName' required='true' /]
	[/@ui.bambooSection]
	[@ui.bambooSection dependsOn='cf_option' showOn='map']
		<p>[@ww.text name='cloudfoundry.task.application.map.description' /]</p>
	    [@ww.textfield labelKey='cloudfoundry.task.application.mapName' descriptionKey='cloudfoundry.task.application.mapName.description' name='cf_mapName' required='true' /]
	    [@ww.textfield labelKey='cloudfoundry.task.application.mapUri' descriptionKey='cloudfoundry.task.application.mapUri.description' name='cf_mapUri' required='true' /]
	[/@ui.bambooSection]
	[@ui.bambooSection dependsOn='cf_option' showOn='unmap']
		<p>[@ww.text name='cloudfoundry.task.application.unmap.description' /]</p>
	    [@ww.textfield labelKey='cloudfoundry.task.application.unmapName' descriptionKey='cloudfoundry.task.application.unmapName.description' name='cf_unmapName' required='true' /]
	    [@ww.textfield labelKey='cloudfoundry.task.application.unmapUri' descriptionKey='cloudfoundry.task.application.unmapUri.description' name='cf_unmapUri' required='true' /]
	[/@ui.bambooSection]
	[@ui.bambooSection dependsOn='cf_option' showOn='rename']
		<p>[@ww.text name='cloudfoundry.task.application.rename.description' /]</p>
	    [@ww.textfield labelKey='cloudfoundry.task.application.renameName' descriptionKey='cloudfoundry.task.application.renameName.description' name='cf_renameName' required='true' /]
	    [@ww.textfield labelKey='cloudfoundry.task.application.renameNewName' descriptionKey='cloudfoundry.task.application.renameNewName.description' name='cf_renameNewName' required='true' /]
		[@ww.checkbox labelKey='cloudfoundry.task.application.rename.failTaskOnNotExists' descriptionKey='cloudfoundry.task.application.rename.failTaskOnNotExists.description' name='cf_renameFailIfNotExists' /]
	[/@ui.bambooSection]
	[@ui.bambooSection dependsOn='cf_option' showOn='appNameSearch']
		<p>[@ww.text name='cloudfoundry.task.application.appNameSearch.description' /]</p>
	    [@ww.textfield labelKey='cloudfoundry.task.application.appNameSearchRegex' descriptionKey='cloudfoundry.task.application.appNameSearchRegex.description' name='cf_appNameSearchRegex' required='true' /]
	    [@ww.textfield labelKey='cloudfoundry.task.application.appNameSearchVariable' descriptionKey='cloudfoundry.task.application.appNameSearchVariable.description' name='cf_appNameSearchVariable' required='true' /]
	[/@ui.bambooSection]
	
[/@ui.bambooSection]