package org.gaptap.bamboo.cloudfoundry.client;

import org.junit.Test;
import reactor.core.publisher.Mono;

import java.util.concurrent.TimeoutException;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.RETURNS_SMART_NULLS;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ConnectionValidatorTest {

    private final CloudFoundryService cloudFoundryService = mock(CloudFoundryService.class, RETURNS_SMART_NULLS);
    private final ConnectionValidator connectionValidator = new ConnectionValidator();

    @Test
    public void invalidConnection(){
        when(cloudFoundryService.validateConnection())
                .thenReturn(Mono.error(new TimeoutException()));

        ConnectionParameters connectionParameters = ConnectionParameters.builder()
                .targetUrl("api.run.pivotal.io")
                .isTrustSelfSignedCerts(true)
                .username("dehringer")
                .password("password")
                .isPasswordEncrypted(false)
                .build();
        assertFalse(connectionValidator.isValid(cloudFoundryService));
    }

    @Test
    public void validConnection(){
        when(cloudFoundryService.validateConnection())
                .thenReturn(Mono.empty());

        ConnectionParameters connectionParameters = ConnectionParameters.builder()
                .targetUrl("api.run.pivotal.io")
                .isTrustSelfSignedCerts(true)
                .username("dehringer")
                .password("password")
                .isPasswordEncrypted(false)
                .build();
        assertTrue(connectionValidator.isValid(cloudFoundryService));
    }
}
